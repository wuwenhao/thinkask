//by：记忆、del QQ：380822670
//note 修复 任意放大时，边线被覆盖问题
//note 修复边框颜色不兼容问题
tinymce.init({
    selector:'#message',
    theme: "modern",
    skin:"sansanyun",
    height: 300,

    plugins: [
        " image media link  emoticons codesample",
        " hr  textcolor colorpicker paste code fullscreen  pagebreak preview",
        "contextmenu directionality  textpattern charmap  lists  autosave"
    ],

      //autolink 自动将内容中url转换成可以点击的 X
      //fullscreen 全屏的时候 带有bar
      //autosave自动保存
        autosave_ask_before_unload: false,
        autosave_interval: "20s",
        autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
        autosave_restore_when_empty: false,
        autosave_retention: "30m",
        //list默认样式
        advlist_bullet_styles: "default", // only include square bullets in list
        advlist_number_styles: "default",
        //右键
        contextmenu: "bold link image",

        //
        textpattern_patterns: [
            {start: '*', end: '*', format: 'italic'},
            {start: '**', end: '**', format: 'bold'},
            {start: '#', format: 'h1'},
            {start: '##', format: 'h2'},
            {start: '###', format: 'h3'},
            {start: '####', format: 'h4'},
            {start: '#####', format: 'h5'},
            {start: '######', format: 'h6'},
            {start: '1. ', cmd: 'InsertOrderedList'},
            {start: '* ', cmd: 'InsertUnorderedList'},
            {start: '- ', cmd: 'InsertUnorderedList'}
        ],

    menubar: false,


        toolbar1: "bold,italic,strikethrough,bullist,numlist,blockquote,alignleft,aligncenter,alignright,image,media,link,unlink,emoticons,codesample,show",
        toolbar2: "formatselect,underline,alignjustify,hr,forecolor,backcolor,pastetext,removeformat,charmap,outdent,indent,pagebreak,preview",
        setup: function (editor) {

            editor.addButton('show', {
                icon: "show",
                tooltip: '显示/隐藏工具栏',
                onPostRender: function() {
                    showbtn = this;
                },
                onclick: function () {
                    isshow();
                }
            });

            function isshow() {
                var state;
                toolbars = editor.theme.panel.find('.toolbar:not(.menubar)');
                if ( ! toolbars || toolbars.length < 2 || ( state === 'hide' && ! toolbars[1].visible() ) ) {
                    return;
                }
                if ( ! state && toolbars[1].visible() ) {
                    state = 'hide';
                }
                if ( state === 'hide' ) {
                    showbtn && showbtn.active( false );
                } else {

                    showbtn && showbtn.active( true );
                }
                toolbars.each( function( toolbar, i ) {
                    if ( i > 0 ) {
                        if ( state === 'hide' ) {
                            toolbar.hide();
                        } else {
                            toolbar.show();
                        }
                    }
                });

            }
            editor.on( 'PostRender', function() {
                isshow();
            });
        },

});
		//功能：获取TinyMce编辑器的内容(html)
        //参数：editorId——编辑器的id
        //返回：内容字符串；如果失败，返回空字符串
        function getTinyMceHtml(editorId)
        {
            return tinymce.activeEditor.getContent({format: 'raw'});
        }
		//功能：获取TinyMce编辑器的内容(去除html)
        //参数：editorId——编辑器的id
        //返回：内容字符串；如果失败，返回空字符串
        function getTinyMceText(editorId)
        {
            return tinymce.activeEditor.getContent({format: 'text'});
        }