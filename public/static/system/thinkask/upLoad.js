// cmzUpload('filePicker2','filePicker2_img','filePicker2_val')

function cmzUpload(inputId,imgId,imgval){
    $('#'+imgId).click(function(event) {
        $('#'+inputId).click();
       // layer.open({
       //      content: '通过style设置你想要的样式',
       //      style: 'background-color:#09C1FF; color:#fff; border:none;',
       //      time: 2
       //  });
    });
        document.querySelector('#'+inputId).addEventListener('change', function () {
        // alert('sfds')
        var that = this;

        lrz(that.files[0], {
            width: 800
        })
            .then(function (rst) {
                var img = new Image(),
                div = document.createElement('div'),
                p = document.createElement('p'),
                sourceSize = toFixed2(that.files[0].size / 1024),
                resultSize = toFixed2(rst.fileLen / 1024),
                scale = parseInt(100 - (resultSize / sourceSize * 100));
                
                // document.querySelector('#'+imgId).src= rst.base64;
                img.src = rst.base64;
                 // /!* ==================================================== *!/
                 // 原生ajax上传代码，所以看起来特别多 ╮(╯_╰)╭，但绝对能用
                 // 其他框架，例如ajax处理formData略有不同，请自行google，baidu。
                 var xhr = new XMLHttpRequest();
                 // xhr.open('POST', '/Api/Cmzupload/up');
                    $.ajax({
                        url: '/Api/Cmzupload/up',
                        type: 'post',
                        dataType: 'json',
                        data: {base64: rst.base64},
                    })
                    .done(function(d) {
                        $('#'+imgId).attr('src', d.info);
                        $('#'+imgval).val(d.info);
                        console.log("success");
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                    
                 xhr.onload = function (d) {
                 if (xhr.status === 200) {
                    // alert(d);
                    for(i in rst ){
                      // alert(i);           //获得属性
                      // alert(d[i]);  //获得属性值

                    }
                    // alert(rst)
                // $('#'+imgId).attr('src', d);
                 // 上传成功
                 } else {
                 // 处理其他情况
                 }
                 };

                 xhr.onerror = function () {
                 // 处理错误
                 };

                 // issues #45 提到似乎有兼容性问题,关于progress
                 xhr.upload.onprogress = function (e) {
                 // 上传进度
                 // var percentComplete = ((e.loaded / e.total) || 0) * 100;
                 };

                 // 添加参数和触发上传
                 // rst.formData.append('base64', rst.base64);
                 // xhr.send(rst.formData);
                 // /!* ==================================================== *!/

                return rst;
            });
    });


}


function toFixed2 (num) {
    return parseFloat(+num.toFixed(2));
}

/**
 * 替换字符串 !{}
 * @param obj
 * @returns {String}
 * @example
 * '我是!{str}'.render({str: '测试'});
 */
String.prototype.render = function (obj) {
    var str = this, reg;

    Object.keys(obj).forEach(function (v) {
        reg = new RegExp('\\!\\{' + v + '\\}', 'g');
        str = str.replace(reg, obj[v]);
    });

    return str;
};

/**
 * 触发事件 - 只是为了兼容演示demo而已
 * @param element
 * @param event
 * @returns {boolean}
 */
function fireEvent (element, event) {
    var evt;

    if (document.createEventObject) {
        // IE浏览器支持fireEvent方法
        evt = document.createEventObject();
        return element.fireEvent('on' + event, evt)
    }
    else {
        // 其他标准浏览器使用dispatchEvent方法
        evt = document.createEvent('HTMLEvents');
        // initEvent接受3个参数：
        // 事件类型，是否冒泡，是否阻止浏览器的默认行为
        evt.initEvent(event, true, true);
        return !element.dispatchEvent(evt);
    }
}
