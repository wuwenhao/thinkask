$(function(){

//======================================================================AJAX=============================================================

//AJAX删除，通用
//<a class="btn btn-danger btn-xs" table="category" where="id-{$v.id}"  href="javascript:;">删除</a>
$('.cmzdel').click(function(event) {
     var table = _getAttr($(this),'table')
    var where = _getAttr($(this),'where')
    var o = {};
    o['table'] = table;
    o['where'] = where;
                    
        layer.confirm('您确定此操作行为吗？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                
                    // cmzAlert(o);
                   cmzAjax('/systems/ajax/delete',"json",o);
            });


});
// 删除分类信息
$('.cmzcatdel').click(function(event) {
     var table = _getAttr($(this),'table')
    var where = _getAttr($(this),'where')
    var o = {};
    o['table'] = table;
    o['where'] = where;
                    
        layer.confirm('您确定此操作行为吗？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                
                    // cmzAlert(o);
                   // cmzAjax('/Ajax/Db/deletecategory',"json",o);
            });


});
/**
 * [锁定文章]
 * @param  {[type]} event) {             } [description]
 * @return {[type]}        [description]
 */
$('.lock').click(function(event) {
    var type = _getAttr($(this),'type')
    var o = {};
    o['type'] = where;
    cmzAlert('锁定文章')
    // cmzAjax('/Ajax/Db/deletecategory',"json",o);
 
});
$('.article-btn').click(function(event) {
  var o = {};
  var comment = $("textarea[name='comment']").val();
  var at_uid = $("textarea[name='comment']").attr('atuid');
  var id = $("textarea[name='comment']").attr('id');
   o['comment'] = comment;
   o['at_uid'] = at_uid;
   o['id'] = id;
  if(!comment){
    cmzAlert('回复内容不能为空',2);
  }else{
    $.ajax({
      url: '/article/ajax/comment',
      type: 'post',
      dataType: 'json',
      data: o,
      beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
      success:function(d){
        if(d.code>0){
          // $('').append('');
          window.location.reload();
        }else{
          var index = layer.load(0, {shade: false,time: 1000}); //0代表加载的风格，支持0-2
          cmzAlert(d.msg,2)

        }
      }
    })
    
    // cmzAjax('',"json",o); 
  }
});
//文章评论编辑
$('.c_edit').click(function(event) {
  var comment = $("textarea[name='edit_comment']").val();
  var o = {};
  o['id'] = $("textarea[name='edit_comment']").attr('id');
  o['comment'] = comment;
if(!comment){
    cmzAlert('回复内容不能为空',2);
  }else{
    $.ajax({
      url: '/article/ajax/comment_edit',
      type: 'post',
      dataType: 'json',
      data: o,
      beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
      success:function(d){
        if(d.code>0){
          // $('').append('');
          window.location.reload();
        }else{
          var index = layer.load(0, {shade: false,time: 1000}); //0代表加载的风格，支持0-2
          cmzAlert(d.msg,2)

        }
      }
    })
  }

 
});
//文章评论点赞
$('.c_zhan').click(function(event) {
  var id = $(this).attr('id');
  var o={};
  o['id'] = id;
   $.ajax({
      url: '/article/ajax/comment_zhan',
      type: 'post',
      dataType: 'json',
      data: o,
      beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
      success:function(d){
        if(d.code>0){
          // $('').append('');
          window.location.reload();
        }else{
          var index = layer.load(0, {shade: false,time: 1000}); //0代表加载的风格，支持0-2
          cmzAlert(d.msg,2)
        }
      }
    })
});
$('.lock').click(function(event) {
    var type = _getAttr($(this),'type')
    var o = {};
    o['type'] = where;
    cmzAlert('锁定文章')
    // cmzAjax('/Ajax/Db/deletecategory',"json",o);
});

//AJAX提交，通用
//AJAX修改，通用
$('.cmzedit').click(function(event) {
        var table = _getAttr($(this),'table')
        var where = _getAttr($(this),'where')
        var returnurl = _getAttr($(this),'returnurl')
        var o = getFormJson('.cmzForm');
        o['table'] = table;
        o['where'] = where;
        o['returnurl'] = returnurl;
       if(!where){
        //HWERE为空，是新加，WHERE不为空是修改
        cmzAjax('/systems/ajax/add',"json",o);
       }else{
        cmzAjax('/systems/ajax/edit',"json",o);
       }
       

});
//表传输加密
$('.cmzEditEncode').click(function(event) {
        var table = _getAttr($(this),'table')
        var where = _getAttr($(this),'where')
        var returnurl = _getAttr($(this),'returnurl')
        var o = getFormJson('.cmzForm');
        o['table'] = table;
        o['where'] = where;
        o['returnurl'] = returnurl;
       
       //HWERE为空，是新加，WHERE不为空是修改
       if(where==""){
         cmzAjax('/ajax/Db/codeadd',"json",o);
       }else{

        cmzAjax('/ajax/Db/codeedit',"json",o);
       }
});

$('.changeStatus').click(function(event) {
        var table = _getAttr($(this),'table')
        var where = _getAttr($(this),'where')
        var from = _getAttr($(this),'from')
        var to = _getAttr($(this),'to')
        to = (to==1)?0:1;
         var o = {};
       o['table'] = table;
       o['where'] = where;
       o['from'] = from;
       o['to'] = to;
       cmzAjax('/ajax/Status/changeStatus',"json",o);
});
$('.logout').click(function(event) {
   var o = {};
  o['status'] = 1;
  cmzAjax('/ucenter/ajax/logout',"json",o);
});

//POST提交，通用
$('.cmzPost').click(function(event) {
   var data = getFormJson('.cmzForm');
    var url = $('.cmzForm').attr('action');
     data['table'] = _getAttr($(this),'table');
      data['returnurl'] = _getAttr($(this),'returnurl');
     $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
        success:function(d){
            if(d.code==1){

                if(d.url==""||!d.url){
                     // location.href = "/";
                    window.location.reload();
                  
               }else{

                     if(d.url=="close-parent-reload"){
                       layer.confirm(d.msg, {
                              btn: ['确定'] //按钮
                          }, function(){
                            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                             //关闭iframe
                             parent.location.reload();
                              parent.layer.close(index);
                              
                          });

                   }else{

                         layer.confirm(d.msg, {
                              btn: ['确定'] //按钮
                          }, function(){
                               location.href = d.url;
                          });
                       
                   }

               }
           }else{
            var index = layer.load(0, {shade: false,time: 1000}); //0代表加载的风格，支持0-2
            cmzAlert(d.msg,2)
           }
            
        }

    })

});
//提交问题回答ajax
//POST提交，通用
$('#comment').click(function(event) {
	$('#message').val(getTinyMceText('tinymce').trim());
	var questionID=$('#questionID').val();
    var url = $('.comment_form').attr('action');
	var data = getFormJson('.comment_form');
	if(getTinyMceText('tinymce').trim() && url && questionID){
		$.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
        success:function(d){
            if(d.code==1){
			//回答成功
			layer.closeAll('loading');
			layer.msg(d.msg, {icon: 1}); 
			//清空编辑器
			tinymce.activeEditor.setContent('');
			window.location.reload();
           }else{
            layer.closeAll('loading');
            cmzAlert(d.msg,2)
           }
        }
    })
	}else{
		cmzAlert('写点东西再提交吧?!',2);
	}
});
//关注问题ajax
//POST提交，通用
    $('#focusBtn').click(function() {
        $.ajax({
            url: "/question/ajax/focus",
            type: 'post',
            dataType: 'json',
            data: {
                question_id:$('#questionID').val()
            },
            success:function(d){
                if(d.code==1){
                    //关注成功
                    layer.msg(d.msg, {icon: 1});
                    window.location.reload();
                }else{
                    layer.msg(d.msg, {icon: 2});
                    window.location.reload();
                }
            }
        })

    });
//======================================================================AJAX=============================================================

//======================================================================其它=============================================================
/**
 * [点击跳转]
 * @param  {[type]} event) {             window.location.href [description]
 * @return {[type]}        [description]
 */
$('cmzJump').click(function(event) {
  window.location.href=$(this).attr('url');
});
//======================================================================其它=============================================================
/**
 * [tip 提示信息]
 * @param  {[type]} event) {             layer.tips('我是另外一个tips，只不过我长得跟之前那位稍有些不一样。', '吸附元素选择器', {  tips: [1, '#3595CC'],  time: 4000});} [description]
 * @return {[type]}        [description]
 */
$('.tip').hover(function(event) {
  msg = $(this).attr('msg');
  att = "#"+$(this).attr('id');
  if(!msg){
    msg = "可以在元素msg上填写要显示的内容"
  }
  if(!att){
    att=".tip";
  }
  layer.tips(msg, att, {
    tips: [1, '#3595CC'],
    time: 4000
  });
});



//IFRAM弹窗打开新页面
// demo:<a href="javascript:;" url="{:U('Userscategory/edit')}" name="新建组织结构" class="btn btn-primary frAlert" type="button"><i class="fa fa-sticky-note"></i> 新建组织结构</a>
$('.frAlert').click(function(event) {
   var name = $(this).attr('name');
   var url = $(this).attr('url');
   var wh = $(this).attr('wh');
   var hi = $(this).attr('hi');
   if(!name){
    name = "信息";
   }
   if(!wh){
    wh="80%"
   }

  frAlert(name,url,wh,"70%");
}); 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//==============    清空缓存                                                                        //==============
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('.clearCache').click(function() {
  $.ajax({
    url: '/systems/ajax/clearcache',
    type: 'post',
    dataType: 'json',
    data: {param1: 'value1'},
    success:function(d){
      if(d.code){
          cmzAlert(d.msg)
      }
    }
  })
 
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//==============     操作文件的AJAX                                                                          //==============
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('.cmzEditFile').click(function(event) {
        var table = _getAttr($(this),'table')
        var where = _getAttr($(this),'where')
        var returnurl = _getAttr($(this),'returnurl')
        var o = getFormJson('.cmzForm');
        o['table'] = table;
        o['where'] = where;
        o['returnurl'] = returnurl;
       
       //HWERE为空，是新加，WHERE不为空是修改
       if(where==""){
         cmzAjax('/ajax/File/add',"json",o);
       }else{

        cmzAjax('/ajax/File/edit',"json",o);
       }
});
/**
 * [cmzAjax description]
 * @param  {[type]} url      [请求的URL]
 * @param  {[type]} dataType [数据返出的方式]
 * @param  {[type]} data     [JSON数据]
 * @return {[type]}          [description]
 * close-parent-reload 关闭父级然后刷新子级 
 */
// function cmzAjax(url,dataType,data){
//     if(!dataType){
//         dataType = "html";
//     }
//     $.ajax({
//         url: url,
//         type: 'post',
//         dataType: dataType,
//         data: data,
//         success:function(d){
//             if(d.code==1){
//                 if(d.url==""){
//                   // setTimeout(function(){location.href = d.url;},1000);
//                   location.href = d.url;
//                     layer.confirm(d.msg, {
//                         btn: ['确定'] //按钮
//                     }, function(){
//                         location.reload();  
//                     });
                  
//                }

//                if(d.url=="close-parent-reload"){
//                    layer.confirm(d.msg, {
//                           btn: ['确定'] //按钮
//                       }, function(){
//                         var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
//                          //关闭iframe
//                          parent.location.reload();
//                           parent.layer.close(index);
                          
//                       });

//                }else{
//                 location.href = d.url;
//                 // setTimeout(function(){location.href = d.url;},1000);
//                 layer.confirm(d.msg, {
//                         btn: ['确定'] //按钮
//                     }, function(){
//                         location.href = d.url;  
//                     });
                    
//                }
               
//            }else{
//             cmzAlert(d.msg,2)
            
//            }
            
//         }
//     })
// }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//==============     操作状态                                                                             //==============
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('.cmzStatus').click(function(event) {
   var table = _getAttr($(this),'table')

        var o = {};
        o['table'] = _getAttr($(this),'table');
        o['where'] = _getAttr($(this),'where');
        o['from'] = _getAttr($(this),'from');
        o['to'] = _getAttr($(this),'to');
        if(_getAttr($(this),'log')){
           if(!_getAttr($(this),'logtable')||!_getAttr($(this),'logcontrol')||!_getAttr($(this),'log')||!_getAttr($(this),'logpresentid')){
              cmzAlert('AJAX数据错误',2);
              return false;
          }
          o['logtable'] = _getAttr($(this),'logtable');

          o['logcontrol'] = _getAttr($(this),'logcontrol');
          o['log'] = _getAttr($(this),'log');
          o['logpresentid'] = _getAttr($(this),'logpresentid');

        }
        //对是否进行确认进行判断
   if($(this).attr('confirm')){
      //是否要确认再提交AJAX
       layer.confirm($(this).attr('confirm'), {
              btn: ['确定','取消'] //按钮
          }, function(){
           cmzAjax('/ajax/Status/changeStatus',"json",o);
          });
                    
   }else{
      cmzAjax('/ajax/Status/changeStatus',"json",o);

   }
});
//复制
$('.copy').click(function(event) {
 $('.copy').zclip({ 
            path: "/static/HPLUS/js/ZeroClipboard.swf", 
            copy: function(){ 
                return $(this).attr('text');
  　　　 　　} 
        }); 
 cmzAlert('此浏览器不支持复制，请手动复制',2);
});

//****************************************右侧弹窗========================================================//
$('.ico_checked').click(function(event) {
  if($(this).attr('checked')=="checked"){
        $(this).removeAttr('checked') 
        $(this).removeClass('fa-check-square-o').addClass('fa-square-o');
  
    }else{
        $(this).attr("checked", "checked");
        $(this).removeClass('fa-square-o').addClass('fa-check-square-o');
    }
});

/**
 * 全选
 * [ico_select_all description]
 * @return {[type]} [description]
 * 未选择状态<i style="cursor:pointer;" class="fa fa-square-o ico_checked checkbox"></i>
 * 已选择状态<i style="cursor:pointer;" class="fa fa-check-square-o ico_checked checkbox"></i>
 */
$('.ico_select_all').click(function(event) {
     $('.checkbox').attr("checked", "checked");
     $('.checkbox').removeClass('fa-square-o').addClass('fa-check-square-o');
  
    // $("[class='checkbox']").attr("checked", "checked");
    // $("[class='checkbox']").removeClass('fa-square-o').addClass('fa-check-square-o');
});

/**
 * 反选
 * [ico_reverse_select description]
 * @return {[type]} [description]
 */

$('.ico_reverse_select').click(function(event) {
  for (var i = $('.checkbox').length; i >= 0; i--) {
      if($('.checkbox').eq(i).attr('checked')=="checked"){
         $('.checkbox').eq(i).removeAttr('checked') 
          $('.checkbox').eq(i).removeClass('fa-check-square-o').addClass('fa-square-o');
        }else{
          $('.checkbox').eq(i).attr("checked", "checked");
          $('.checkbox').eq(i).removeClass('fa-square-o').addClass('fa-check-square-o');
        }
  }

      
});




//****************************************右侧弹窗========================================================//
//****************************************右侧弹窗========================================================//
    // var i = -1;
    // var toastCount = 0;
    // var $toastlast;
    // var getMessage = function() {
    //     var msg = "Hi, welcome to Inspinia. This is example of Toastr notification box.";
    //     return msg
    // };
    // $(".cmzStopAlert").click(function() {
    //   var msg = $(this).attr('msg');
    //     toastr.success(msg);
    // });
    // $(".cmzTopAlert").click(function() {
    //     var shortCutFunction = $("#toastTypeGroup input:radio:checked").val();
    //     var msg = $("#message").val();
    //     var title = $("#title").val() || "";
    //     var $showDuration = $("#showDuration");
    //     var $hideDuration = $("#hideDuration");
    //     var $timeOut = $("#timeOut");
    //     var $extendedTimeOut = $("#extendedTimeOut");
    //     var $showEasing = $("#showEasing");
    //     var $hideEasing = $("#hideEasing");
    //     var $showMethod = $("#showMethod");
    //     var $hideMethod = $("#hideMethod");
    //     var toastIndex = toastCount++;
    //     toastr.options = {
    //         closeButton: $("#closeButton").prop("checked"),
    //         debug: $("#debugInfo").prop("checked"),
    //         progressBar: $("#progressBar").prop("checked"),
    //         positionClass: $("#positionGroup input:radio:checked").val() || "toast-top-right",
    //         onclick: null
    //     };
    //     if ($("#addBehaviorOnToastClick").prop("checked")) {
    //         toastr.options.onclick = function() {
    //             alert("You can perform some custom action after a toast goes away")
    //         }
    //     }
    //     if ($showDuration.val().length) {
    //         toastr.options.showDuration = $showDuration.val()
    //     }
    //     if ($hideDuration.val().length) {
    //         toastr.options.hideDuration = $hideDuration.val()
    //     }
    //     if ($timeOut.val().length) {
    //         toastr.options.timeOut = $timeOut.val()
    //     }
    //     if ($extendedTimeOut.val().length) {
    //         toastr.options.extendedTimeOut = $extendedTimeOut.val()
    //     }
    //     if ($showEasing.val().length) {
    //         toastr.options.showEasing = $showEasing.val()
    //     }
    //     if ($hideEasing.val().length) {
    //         toastr.options.hideEasing = $hideEasing.val()
    //     }
    //     if ($showMethod.val().length) {
    //         toastr.options.showMethod = $showMethod.val()
    //     }
    //     if ($hideMethod.val().length) {
    //         toastr.options.hideMethod = $hideMethod.val()
    //     }
    //     if (!msg) {
    //         msg = getMessage()
    //     }
    //     $("#toastrOptions").text("Command: toastr[" + shortCutFunction + ']("' + msg + (title ? '", "' + title: "") + '")\n\ntoastr.options = ' + JSON.stringify(toastr.options, null, 2));
    //     var $toast = toastr[shortCutFunction](msg, title);
    //     $toastlast = $toast;
    //     if ($toast.find("#okBtn").length) {
    //         $toast.delegate("#okBtn", "click",
    //         function() {
    //             alert("you clicked me. i was toast #" + toastIndex + ". goodbye!");
    //             $toast.remove()
    //         })
    //     }
    //     if ($toast.find("#surpriseBtn").length) {
    //         $toast.delegate("#surpriseBtn", "click",
    //         function() {
    //             alert("Surprise! you clicked me. i was toast #" + toastIndex + ". You could perform an action here.")
    //         })
    //     }
    // });
    // function getLastToast() {
    //     return $toastlast
    // }
    // $("#clearlasttoast").click(function() {
    //     toastr.clear(getLastToast())
    // });
    // $("#cleartoasts").click(function() {
    //     toastr.clear()
    // })

//****************************************右侧弹窗========================================================//

//****************************************菜单和TAB导航处理========================================================//
//iframe子级时处理
$('.J_menu_self').click(function(event) {
    var topTab = '<a href="javascript:;" class="active J_menuTab" data-id="'+$(this).attr('url');+'">'+$(this).html()+' <i class="fa fa-times-circle"></i></a>';
    $(window.parent.document).find(".page-tabs-content .J_menuTab").appendTo(topTab)
});

})