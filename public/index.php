<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
define('PLUS_PATH', __DIR__ .'/../plus/');
// 加载框架引导文件
require __DIR__ . '/../core/start.php';
define('THINKASK_VERSION', 'v1.0');


//加上下面这一句话，用于自动加载QueryList
// require __DIR__ . '/../extend/lib/querylist/vendor/autoload.php';
