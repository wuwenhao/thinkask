<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\article\controller;
use app\common\controller\Base;

class Admin extends base
{
  public function list(){
  	$this->powergroup();
  	$this->assign('category',$category = model('Base')->getall('category'));
 	$where=[];
 	if(current($this->request->only(['keyword']))) $where['title']=['like','%'.trim(current($this->request->only(['keyword']))).'%'];;
 	if(current($this->request->only(['category_id']))) $where['category_id']=current($this->request->only(['category_id']));
 	if(current($this->request->only(['user_name']))) $where['user_name']=current($this->request->only(['user_name']));
    $this->assign('list',$re = model('Base')->getpages('article',['where'=>$where,'alias'=>'qu','join'=>[[config('prefix').'users u','u.uid=qu.uid']]]));
    if($where){
    	$this->assign('searchcount',count($re));

    }
  	return $this->fetch('article/admin/list');
  }

}
