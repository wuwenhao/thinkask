<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\topic\controller;
use app\common\controller\Base;

class Admin extends base
{
	 protected  function _initialize(){
	 	parent::_initialize();
	   $this->powergroup();
	 }
	   /**
		 * [list 列表]
		 * @return [type] [description]
		 */
	  public function list(){
		  $where=[];
		  if(!empty(current($this->request->only(['keyword'])))) $where['topic_title']=['like','%'.trim(current($this->request->only(['keyword']))).'%'];;
		  // if(!empty(current($this->request->only(['user_name'])))) $where['user_name']=current($this->request->only(['user_name']));
		  	$this->assign('list',$this->getbase->getpages('topic',['where'=>$where]));
		  	return $this->fetch('topic/admin/list');
	  }
	  /**
	   * [parent 根话题 ]
	   * @return [type] [description]
	   */
	 public function  parent(){

	 
	  	return $this->fetch('topic/admin/parent');
	  }
	  /**
	   * [creat 新建话题]
	   * @return [type] [description]
	   */
	 public function  creat(){

	 
	  	return $this->fetch('topic/admin/creat');
	  }

}
