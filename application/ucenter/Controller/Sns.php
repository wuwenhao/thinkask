<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\ucenter\controller;
use app\common\controller\Base;

    // Header('Content-Type: text/html; charset=utf-8');
    class Sns extends Base
    {
        public function _initialize() {
               
         }


    /**
     * [auth 定义公共方法，作用为访问Ta进入三方登录页面]
     * @return [type] [description]
     * type参数 支持 qq sina
     */
    public function auth($type="qq") {
        $sdk = include(EXTEND_PATH."lib/api/Oauth.php");
        $sdk = include(EXTEND_PATH."lib/api/sdk/Qq.php");
        //默认QQ登陆
        $type = isset($_GET['type'])&&!empty($_GET['type'])?I('type'):"qq";
        // 获取QQ登录的接口对象
        $sdk = \extend\lib\api\Oauth::getInstance($type);
        // 重写到模块三方登录的页面
        // $this->redirect($sdk->getRequestCodeURL());
    }

    public function cbsina(){
        // 获取GET传入的code值
        $code = I('code');
        // 设置类型为QQ
        $type = 'sina';
        // 获取QQ的实例化对象
        $sns = \Common\Lib\Api\ThinkOauth::getInstance($type);
        // 给extend赋值初始null
        $extend = null;
        // 获取token值。
        $token = $sns->getAccessToken($code, $extend);
        $openid = $token['openid'];
        
        // 判断token是否是数组
        if (is_array($token)) {

             $localuserinfo = M('users')->where("sina_oid = '{$openid}'")->find();
            if(count($localuserinfo)>1){
                usersession($localuserinfo);
                $this->success('登陆成功','/');
            }else{
                // 获取用户的详细信息
              $data = $sns->call('users/show',array('uid'=>$openid));
                // 将获取到的信息进行整理
                if ($data['ret'] == 0) {
                    $userInfo['login_type'] = 'sina';
                    $userInfo['loginName'] = $data['name']."_sina";
                    $userInfo['sina_nickname'] = $data['name'];
                    $userInfo['sina_header'] = $data['avatar_large'];
                    $userInfo['sina_oid'] = $openid;
                    M('users')->add($userInfo);
                     usersession(M('users')->where("sina_oid = '{$openid}'")->find());
                    $this->success('登陆成功','/');
                    //判断用户信息是否存在，不存在插入表中，存在的话就直接生成SESSION
                    
                    

                    // 此处的$userInfo就是需要的用户信息
                } else {
                    E('获取新浪用户信息失败 : ' . $data['msg']);
                }
            }


            
        } else {
            E('获取新浪用户信息失败  ');
        }

    }


    /**
     * QQ登录的回调页面
     */
    public function cbqq() {
        // 获取GET传入的code值
        $code = I('code');
        // 设置类型为QQ
        $type = 'QQ';
        // 获取QQ的实例化对象
        $sns = \Common\Lib\Api\ThinkOauth::getInstance($type);
        // 给extend赋值初始null
        $extend = null;
        // 如果是腾讯微博的客户，此处给extend重新赋值
        if ($type == 'tencent') {
            $extend = array('openid' => $this->_get('openid'), 'openkey' => $this->_get('openkey'));
        }
        // 获取token值。
        $token = $sns->getAccessToken($code, $extend);
        $openid = $token['openid'];

        // 判断token是否是数组
        if (is_array($token)) {

             $localuserinfo = M('users')->where("qq_oid = '{$openid}'")->find();
            if(count($localuserinfo)>1){
                usersession($localuserinfo);
                $this->success('登陆成功','/');
            }else{
                // 获取用户的详细信息
                $data = $sns->call('user/get_user_info');
                // 将获取到的信息进行整理
                if ($data['ret'] == 0) {
                    $userInfo['login_type'] = 'qq';
                    $userInfo['loginName'] = $data['nickname']."_qq";
                    $userInfo['qq_nickname'] = $data['nickname'];
                    $userInfo['qq_header'] = $data['figureurl_2'];
                    $userInfo['qq_oid'] = $openid;
                    M('users')->add($userInfo);
                     usersession(M('users')->where("qq_oid = '{$openid}'")->find());
                    $this->success('登陆成功','/');
                    //判断用户信息是否存在，不存在插入表中，存在的话就直接生成SESSION
                    
                    

                    // 此处的$userInfo就是需要的用户信息
                } else {
                    E('获取腾讯QQ用户信息失败 : ' . $data['msg']);
                }
            }


            
        } else {
            E('获取腾讯QQ用户信息失败  ');
        }
       
    }



    }