<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\post\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Hook;
class Publish extends controller
{

	public function _initialize()
    {
	 if((int)Session::get('thinkask_uid')>0){
	            $this->assign('uid',Session::get('thinkask_uid'));
	            $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
	            $this->assign('userinfo',$userinfo);
		     	//分类
		     	$this->assign('category',model('Category')->getall());
			      //分组
			    $this->assign('group',model('Group')->getall());
	        }else{
	            $this->assign('uid',0);
	     }
       
    }
/**
 * [article 文章]
 * @return [type] [description]
 */
  public function article(){
  	$id=is_array($this->request->only(['id']))?current($this->request->only(['id'])):0;
  	if($id>0){
  		$this->assign(model('Base')->getone('article',['where'=>"id={$id}",'cache'=>false]));
  	}
  	return $this->fetch('publish/article');
  }
  /**
   * [question 问题]
   * @return [type] [description]
   */
  public function question(){
  	$id=is_array($this->request->only(['id']))?current($this->request->only(['id'])):0;
  	if($id>0){
  		$this->assign(model('Base')->getone('question',['where'=>"question_id={$id}",'cache'=>false]));
  	}
  	return $this->fetch('publish/question');

  }
}
