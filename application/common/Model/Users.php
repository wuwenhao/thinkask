<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\model;
use think\Model;
use think\Db;
class Users extends Model
{
    static public function getUserByUid($uid){
    return $re = Db::name('users')->where('uid',$uid)->find();
    }
    
    /**
     * 获取数据表所有内容
     */
    public function getAllList($table)
    {
    	return Db::name($table)->select();
    }
	
	public function update_avatar($data){
		
       return  Db::name('users')->where('uid',$data['uid'])->update($data);
    }
}