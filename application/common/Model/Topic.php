<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\model;
use think\Model;
use think\Db;
class Topic extends Model
{  
    protected $request;
    /**
     * [getList 列表]
     * @param  string $status [description]
     * @return [type]         [description]
     */
    static public function getList($status="news")
    { 
        switch ($status) {
                
                default:
                    $order="topic_id desc";
                    break;
            }
        $join = [
                ];
    	
    return $list = Db::name('topic')->alias('a')->order($order)->join($join)->select();

    }
    
    /**
     * 获取数据表所有内容
     */
    public function getAllList($table)
    {
    	return Db::name($table)->select();
    }
}