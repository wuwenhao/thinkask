<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\controller;

abstract class Plus extends Base
{
/**
 * [display 分配模板]
 * @param  string $template [description]
 * @param  array  $vars     [description]
 * @param  array  $replace  [description]
 * @param  array  $config   [description]
 * @return [type]           [description]
 */
  public function display($template = '', $vars = [], $replace = [], $config = []) {
        $plusname = strtolower(tostr($this->request->only(['plusname'])));
        $controller = strtolower(tostr($this->request->only(['controller'])));
        $action = strtolower(tostr($this->request->only(['action'])))?strtolower(tostr($this->request->only(['action']))):"index";
        $template = $template?$template:$action;
        // show($template);
     echo $this->fetch("../../plus/".$plusname."/view/".$controller."/".$template,$vars, $replace, $config);
  }

    // final protected function theme($theme){
    //     $this->view->theme($theme);
    //     return $this;
    // }
    

    // final protected function assign($name,$value='') {
    //     $this->view->assign($name,$value);
    //     return $this;
    // }


    // //用于显示模板的方法
    // final protected function fetch($templateFile = 'widget'){
    //     if(!is_file($templateFile)){
        	
    //     	$config=$this->getConfig();
    //     	$theme=$config['theme'];
        	
    //     	$depr = "/";
        	
    //     	$theme=empty($theme)?"":$theme.$depr;
        	
    //         $templateFile = sp_add_template_file_suffix("./".$this->tmpl_root.$templateFile);
    //         if(!file_exists_case($templateFile)){
    //             throw new \Exception("模板不存在:$templateFile");
    //         }
    //     }
    //     return $this->view->fetch($templateFile);
    // }

    // final public function getName(){
    //     $class = get_class($this);
    //     return substr($class,strrpos($class, '\\')+1, -6);
    // }

    // final public function checkInfo(){
    //     $info_check_keys = array('name','title','description','status','author','version');
    //     foreach ($info_check_keys as $value) {
    //         if(!array_key_exists($value, $this->info))
    //             return false;
    //     }
    //     return true;
    // }

    // /**
    //  * 获取插件的配置数组
    //  */
    // public function getConfig($name=''){
    	
    //     static $_config = array();
    //     if(empty($name)){
    //         $name = $this->getName();
    //     }
    //     if(isset($_config[$name])){
    //     	return $_config[$name];
    //     }
        
    //     $config=M('Plugins')->where(array("name"=>$name))->getField("config");

    //     if(!empty($config) && $config!="null"){
    //         $config   =   json_decode($config, true);
    //     }else{
    //         $config=array();
    //         $temp_arr = include $this->config_file;
    //         if(!empty($temp_arr)){
    //             foreach ($temp_arr as $key => $value) {
    //                 if($value['type'] == 'group'){
    //                     foreach ($value['options'] as $gkey => $gvalue) {
    //                         foreach ($gvalue['options'] as $ikey => $ivalue) {
    //                             $config[$ikey] = $ivalue['value'];
    //                         }
    //                     }
    //                 }else{
    //                     $config[$key] = $temp_arr[$key]['value'];
    //                 }
    //             }
    //         }
            
    //     }
    //     $_config[$name]     =   $config;
    //     return $config;
    // }

    //必须实现安装
    abstract public function install();

    //必须卸载插件方法
    abstract public function uninstall();
}
