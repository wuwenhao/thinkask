<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use app\common\controller\Base;
class Ajax extends Base{
/**
* [saveconfig 保存配置项目]
* @return [type] [description]
*/
public function saveconfig(){
    if($this->request->isAJax()){
        $request = $this->request->param();
        foreach ( $request as $k => $v) {
            //修改
            if($this->getbase->getone('system_setting',['where'=>['varname'=>$k]])){
                $data['value'] = serialize($v);
                $this->getbase->getedit('system_setting',['where'=>['varname'=>$k]],$data);
            }else{
                //新加
                $data['value'] = serialize($v);
                $data['varname'] = $k;
                $this->getbase->getadd('system_setting',$data);
            }
        }
        cache('system_setting',model('Base')->getall('system_setting',['order'=>'id asc','cache'=>false]));
        $this->success(lang('update_success'),'');
    }
    


 }
/**
 * [testmail 邮件测试]
 * @return [type] [description]
 */
public function testmail(){
    $set = getset('mail_config');
    json(send_mail($set['testmail']));
 }

}
